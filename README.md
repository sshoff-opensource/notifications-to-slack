# Notifications to Slack

## Description
Service for sending notifications to a Slack *public* channel or to a Slack *private* channel after inviting the bot to it from a **non-public** backend.

**CAUTION**: The service doesn't request any auth for requests! Don't do the service public accessible!

## Installation

### Sentry
If you want to use Sentry for error catching, create a new project at [sentry.io](https://sentry.io/) and provide the given DSN via the environment variable `SENTRY_DSN`.

### Create a Slack App

1. Create a new [Slack App](https://api.slack.com/apps?new_granular_bot_app=1).

1. Navigate to **OAuth & Permissions** and scroll down to the section for **Scopes**. In the subsection **Bot Token Scopes** click on the button **Add an OAuth Scope**. Chose the scopes `chat:write` and `chat:write.public`.

1. Scroll up at this page (**OAuth & Permissions**) to the top of the page to section **OAuth Tokens for Your Workspace** and click the **Install to Workspace** button. Allow installation.

1. Copy **Bot User OAuth Token** (it started from *xoxb*) and insert into the environment variable `SLACK_TOKEN`.

1. Create a new channel and add the bot to it: `/invite @YourBotUser`.

## Usage

### Environment variables

#### Obligatory

```
SLACK_TOKEN="xoxb-..."
```

#### Optional

```
PORT=4050
SENTRY_DSN="https://qwe@o123.ingest.sentry.io/456"
```

### Request

```
$ curl --location 'http://127.0.0.1:4050/' \
--header 'Content-Type: application/json' \
--data '{
    
    "channel": "channel-name",
    "message": "Message to send"
}'
```

### In development mode

[Node Slack SDK documentation](https://slack.dev/node-slack-sdk/getting-started).

```
$ PORT=4050 SLACK_TOKEN="xoxb-..." SENTRY_DSN="https://qwe@o123.ingest.sentry.io/456" node app.js
```

OR
```
$ export PORT=4050
$ export SLACK_TOKEN="xoxb-..."
$ export SENTRY_DSN="https://qwe@o123.ingest.sentry.io/456"
```

And after it just:
```
$ node app.js
```


## Support
[Issue tracker](https://gitlab.com/sshoff-opensource/notifications-to-slack/-/issues).

## Authors and acknowledgment
The author is Sergey SH <sshoff@sshoff.com>.

## License
[MIT](https://en.wikipedia.org/wiki/MIT_License).

## Project status
Maintaining.
