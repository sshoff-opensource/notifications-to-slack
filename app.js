import express from 'express';
import * as Sentry from "@sentry/node";
import { WebClient } from "@slack/web-api";

const app = express();
const port = parseInt(process.env.PORT) || 4050;

const sentryDSN = process.env.SENTRY_DSN;
const slackToken = process.env.SLACK_TOKEN;

if (sentryDSN) {
    Sentry.init({
        dsn: sentryDSN,
        integrations: [
            // enable HTTP calls tracing
            new Sentry.Integrations.Http({ tracing: true }),
            // enable Express.js middleware tracing
            new Sentry.Integrations.Express({ app }),
            // Automatically instrument Node.js libraries and frameworks
            ...Sentry.autoDiscoverNodePerformanceMonitoringIntegrations(),
        ],

        // Set tracesSampleRate to 1.0 to capture 100%
        // of transactions for performance monitoring.
        // We recommend adjusting this value in production
        tracesSampleRate: 1.0,
    });

    // RequestHandler creates a separate execution context, so that all
    // transactions/spans/breadcrumbs are isolated across requests
    app.use(Sentry.Handlers.requestHandler());
    // TracingHandler creates a trace for every incoming request
    app.use(Sentry.Handlers.tracingHandler());

    // All controllers should live here
}

app.use(express.json());

app.post('/', async (req, res) => {
    if (!slackToken) {
        error(res, 500, 'Slack Token must be provided via the environment variable SLACK_TOKEN!');
    }

    let channel = '';
    let message = '';

    if (!req.body.channel) {
        error(res, 500, 'The `channel` parameter in the JSON from a request body is obligatory.');
    } else {
        channel = req.body.channel;
    }

    if (!req.body.message) {
        error(res, 500, 'The `message` parameter in the JSON from a request body is obligatory.');
    } else {
        message = req.body.message;
    }

    if (channel !== '' && message !== '') {
        const slackClient = new WebClient(slackToken);

        try {
            await slackClient.chat.postMessage({
                channel: channel,
                text: message,
            });

            successResponse(res, 'Message posted.');
        } catch (err) {
            error(res, 400, err);
        }
    } else {
        error(res, 400, 'The parameter `channel` or `message` in the JSON from a request body is empty.');
    }
});

if (sentryDSN) {
    // The error handler must be before any other error middleware and after all controllers
    app.use(Sentry.Handlers.errorHandler());

    // Optional fallthrough error handler
    app.use(function onError(err, req, res, next) {
        // The error id is attached to `res.sentry` to be returned
        // and optionally displayed to the user for support.
        res.statusCode = 500;
        res.end(res.sentry + "\n");
    });
}

app.listen(port, () => {
    console.log(`Service started successfully on port ${port}.`);
});

function successResponse(res, msg) {
    const response = {
        status: 'ok',
        result: msg
    };

    res.status(200).json(response);

}

function error(res, status, msg) {
    const err = new Error();

    err.status = 'error';
    err.message = msg;

    res.status(status).json(err);
}
